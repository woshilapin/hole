#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'woshilapin'
SITENAME = 'In the rabbit hole'
SITEURL = 'https://hole.tuziwo.info'

PATH = 'content/'
OUTPUT_PATH = 'public/'
STATIC_PATHS = [
        'files/',
        'images/',
        ]

PLUGIN_PATHS = ['pelican-plugins/']
PLUGINS = ['asciidoc_reader']
ASCIIDOC_CMD = 'asciidoctor'
ASCIIDOC_OPTIONS = ['--base-dir={}'.format(PATH), '--attribute source-highlighter=rouge']
TYPOGRIFY = True

LOAD_CONTENT_CACHE = True
CACHE_PATH = '.pelican-cache/'
CACHE_CONTENT = True
CONTENT_CACHING_LAYER = 'reader'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'en'

# Blogroll
LINKS = (('Pelican', 'http://getpelican.com/'),
         ('Python.org', 'http://python.org/'),
         ('Jinja2', 'http://jinja.pocoo.org/'),)

# Social widget
SOCIAL = (
        ('gitlab', 'https://gitlab.com/woshilapin'),
        ('github', 'https://github.com/woshilapin'),
        ('stackoverflow', 'https://stackoverflow.com/users/7447059/woshilapin'),
        ('linkedin', 'https://www.linkedin.com/in/simard-jean/'),
        ('mastodon', 'https://mamot.fr/@woshilapin'),
        ('twitter', 'https://twitter.com/woshilapin'),
)

DEFAULT_PAGINATION = 10
DISPLAY_CATEGORIES_ON_MENU = False

# Uncomment when doing local development
RELATIVE_URLS = True
