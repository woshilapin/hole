= About
:author: woshilapin
:email: woshilapin@tuziwo.info
:date: 18-05-2020
:slug: about
:category: pages
:lang: en
:tags: blog

== Who am I?

I'm Jean SIMARD, I'm passionate about computer programming.  I believe I started
this journey creating programs on my Casio 100 on which my first real
accomplishment was some sort of 3D engine to render graphically functions with 2
variables.  Then a friend helped me start coding in Turbo Pascal.  This
is when I started University and studied Computer Sciences.

University was the opportunity to jump in the Linux world and open source
communities.  I actually became fond of
link:https://en.wikipedia.org/wiki/LaTeX[LaTeX] at University thanks to a signal
processing teacher.  And I obviously learned tons of stuff about computers and
programming.

Later on, I was able to find a job in a company which edits an excellent
open-source wiki solution: link:https://www.xwiki.org/[XWiki].  This was my Java
period, but it was also the time when I understood much more about programming
communities, usefulness of testing and documenting (thanks you
link:https://massol.myxwiki.org/xwiki/bin/view/XWiki/VincentMassol[Vincent]),
agility and hacking (thank you link:https://github.com/cjdelisle[Caleb]) and
probably a lot of other things.

== My pseudo story?

In French (I am French), Jean is sometimes associated with
link:https://fr.wikipedia.org/wiki/Le_Conte_de_Jeannot_Lapin[_Le Conte de Jeannot
Lapin_] which in English is called
link:https://fr.wikipedia.org/wiki/The_Tale_of_Benjamin_Bunny[The Tale of
Benjamin Bunny].  This is where come from one of my surname, _lapin_ (which
means "rabbit" in English).

But then why _woshilapin_?  Well, you can split it in _wo_-_shi_-_lapin_.  Now
that you know about _lapin_, _wo_-_shi_ actually is the
link:https://en.wikipedia.org/wiki/Pinyin[Pinyin] transcription of the Chinese
我是 meaning "I am".  So _woshilapin_ means "I am rabbit" litterally.  But now I
guess you're wondering why Chinese?  That is something you'll have to find out
by knowing me personally I guess!

== Why the name of the blog?

As you noticed, the name of the blog is "In the Rabbit Hole", hosted on
hole.tuziwo.info.  Since you now know about my pseudo, I guess the name of the
blog is pretty obvious.  Maybe the less obvious part is the domain name on which
it is hosted: _tuziwo_.  Here again, _tuziwo_ is the
link:https://en.wikipedia.org/wiki/Pinyin[Pinyin] transcription of the Chinese
兔子窝 meaning "Rabbit Hole".
