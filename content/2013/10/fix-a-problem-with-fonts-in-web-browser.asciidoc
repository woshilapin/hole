= Fix a problem with fonts in web browser
:author: woshilapin
:email: woshilapin@gmail.com
:date: 29-10-2013
:slug: fix-a-problem-with-fonts-in-web-browser
:category: tech
:lang: en
:tags: archlinux, web, font

== The problem

In your browser, you may have problems on some websites displaying strange
fonts.  For example, you may have seen the following fonts on Github.

image::images/font-problem.png["Font problem in web browser",width=800]

This problem is due to bitmap font.

== Solution

To deactivate bitmap fonts, you can use existing font configuration files you
can find into `/etc/fonts/conf.avail`.  To avoid bitmap fonts, you just need to
link the `70-no-bitmaps.conf` into `/etc/fonts/conf.d` directory.

	$ cd /etc/fonts
	$ ln -s conf.avail/70-no-bitmaps.conf conf.d/

Now, you can restart your X server for this new configuration taking effect.

== References

- link:https://wiki.archlinux.org/index.php/Font_Configuration#Disable_bitmap_fonts[Arch
  font configuration (tip on bitmap fonts)]
