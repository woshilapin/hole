= Install Eclipse on GNOME 3
:author: woshilapin
:email: woshilapin@gmail.com
:date: 05-11-2013
:slug: install-eclipse-on-gnome-3
:category: tech
:lang: en
:tags: archlinux, gnome, eclipse

== Introduction

I will install Eclipse and add an entry to GNOME to make Eclipse an application.
I use the ``Eclipse IDE for Java EE Developpers'' version and I'm currently
using GNOME 3.10.

== Download and install Eclipse

To download Eclipse, you can go to http://eclipse.org/downloads/.  Once it's
downloaded, you can extract it to `/opt/`.  You should have the binary of
Eclipse in `/opt/eclipse/eclipse`.  You may extract Eclipse into
`/opt/eclipse-4.3.0` and create a symbolic link; this way, you can easily install a new
version of eclipse by just modifying the link.

	$ cd /opt
	$ ln -s eclipse-4.3.0/ eclipse

== Download a SVG icon for Eclipse

In order to make a cute icon in the GNOME launcher, we can also download an icon
(in SVG format) for Eclipse.

	$ cd /opt/eclipse/
	$ curl -o /usr/share/pixmaps/eclipse.svg https://projects.archlinux.org/svntogit/packages.git/plain/trunk/eclipse.svg\?h\=packages/eclipse

== Create the launcher for GNOME

To create the launcher, you need to create the following file 
`/usr/share/applications/eclipse.desktop`.

[source,txt]
-----
include::files/eclipse.desktop[/usr/share/applications/eclipse.desktop]
-----

This is it.  You can now try it!

== References

- link:http://www.surlyjake.com/blog/2011/12/30/create-a-custom-application-launcher-in-gnome3/[Create
  a Custom Application Launcher on GNOME3]
