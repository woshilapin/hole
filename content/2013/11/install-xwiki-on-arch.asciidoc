= Install XWiki on Arch Linux
:author: woshilapin
:email: woshilapin@gmail.com
:date: 05-11-2013
:slug: install-xwiki-on-arch
:category: tech
:lang: en
:tags: archlinux, java, xwiki

== Introduction

In this article, I detail the installation of Java and XWiki on an freshly
installed Arch Linux.  I will use the open source implementation of Java,
OpenJDK and the generic installer of XWiki 5.2.

== Install Java

On Arch Linux with OpenJDK, you need to install the `jre7-openjdk` and
`jdk7-openjdk` packages.

	$ pacman -S jre7-openjdk jdk7-openjdk

== Install XWiki

You first need to download the generic installed on
link:http://enterprise.xwiki.org/xwiki/bin/view/Main/Download[xwiki.org].

Once it is done, you can launch the installer *with root privileges*.

	$ java -jar xwiki-enterprise-installer-generic-5.2-standard.jar

You can now follow the instruction.  I choose to install XWiki into
`/opt/xwiki-5.2`.  If it is your first installation, you should install the
*Default Wiki* because it will create a bunch of useful wiki pages to start.

When installation is finished, the installer asks for icon creation.  I disable
all icon creation because we do not really need them.

To finish this installation, I created a symbolic link on the directory of XWiki
installation.

	$ cd /opt
	$ ln -s xwiki-5.2/ xwiki

== Launch XWiki

To launch XWiki, you have a start script into the freshly installed directory.

	$ cd /opt/xwiki
	$ sh start_xwiki.sh

You should now be able to access your installed XWiki on http://localhost:8080
and enjoy!

== Bonus
=== Log in

To log in this newly installed XWiki, you may use the default account:

- login: ``Admin''
- password: ``admin''

=== Applications for developers

If you want to develop some new applications for XWiki, you may install the
following applications:

- Collection Application: Makes easier the creation of a XAR file
- Syntax Highlighting Application: Syntax highlighting for source, even for
  Velocity
- AutoCompletion Application: Autocompletion in Velocity code

To install them, you need to be logged in with the ``Admin'' accound.  Then,
click on ``More applications'' in the panel on the right.  You will see
``Install new applications''; you can click on it.

Then search for each extension and click the ``Install'' button on the right
then the ``Continue'' button.

=== Reveal hidden pages

By default, XWiki hide some pages, mainly pages that are for developers and
should not be displayed to a lambda user when using the search field (on the
top-right).  However, this could difficult to find some useful pages for a
developer.

To make these page visible, you need to be logged in with the ``Admin'' account.
Then go in the ``Preferences'' in the profile (right in the top-bar).  Click on
the edit button (the little pen of the top right of the gray frame).  You will
have the possibility to change the property ``DISPLAY HIDDEN DOCUMENTS'' to
``Yes''.

You may also change the ``DEFAULT EDITOR TO USE'' to ``Text'' if you do not want
WYSIWYG edition by default.

== References

- link:http://platform.xwiki.org/xwiki/bin/view/AdminGuide/InstallationStandalone[Installing
  the Standalone Distribution]
