#!/bin/awk -f

# Before program
BEGIN {
}

# Program
{
	print "# " $0;
	header = 0;
	lastpatt = "";
	for(k=1; k<=NF; k++) {
		if($k ~ /BLOB|BOOLEAN/) {
			subin="\\(^INSERT INTO \042\\?" ENVIRON["DB"] "\042\\? VALUES(";
			subout = "";
			can_be_null=0;
			subnum=1;
			for(i=1; i<=NF; i++) {
				patt = "";
				if(($i !~ /KEY/) && (i != 1)) {
					subin = subin ",";
				}
				if($i ~ /INTEGER/) {
					patt = "[[:digit:]]\\+";
				}
				if($i ~ /NUMERIC/) {
					patt = "[[:digit:].-]\\+";
				}
				if(($i ~ /VARCHAR|(LONG)?TEXT|BLOB/)) {
					patt = "\047.*\047";
				}
				if(($i !~ /NOT NULL|KEY/)) {
					patt = "\\(NULL\\|" patt "\\)";
					subnum = subnum + 1;
				}
				if($i ~ /BLOB/) {
					if (i < k) {
						patt = "decode(" patt ",\047hex\047)";
					}
					if(i == k) {
						if(patt ~ /NULL/) {
							bdecnum = 1;
							decnum = subnum;
							adecnum = decnum + 1;
							patt = "\\)" patt "\\(";
						} else {
							bdecnum = 1;
							decnum = subnum + 1;
							adecnum = decnum + 1;
							patt = "\\)\\(" patt "\\)\\(";
						}
						subout = "\\" bdecnum "decode(\\" decnum ",\047hex\047)\\" adecnum;
					}
				}
				if($i ~ /BOOLEAN/) {
					patt = "[01]";
					if(i == k) {
						bdecnum = 1;
						decnum = subnum + 1;
						adecnum = decnum + 1;
						patt = "\\)\\(" patt "\\)\\(";
						subout = "\\" bdecnum "\047\\" decnum "\047\\" adecnum;
					}
				}
				if(($i !~ /KEY/)) {
					lastpatt = patt;
				}
				subin = subin patt;
				can_be_null = 0;
			}
			subin = subin ");$\\)";
			if(header == 0) {
				startpatt = ENVIRON["START_PATT"];
				endpatt = lastpatt ENVIRON["END_PATT"];
				print "/" startpatt "/,/" endpatt "/ {";
				print "/" startpatt "/h";
				print "/" startpatt "/!H";
				print "/" endpatt "/ {";
				print "g";
				header = 1;
			}
			print "s/" subin "/" subout "/g";
		}
	}
	print "p";
	print "}";
	print "d";
	print "}";
}

# After program
END {
}
