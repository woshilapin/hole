#!/bin/sh
# The original database
SRC_FILE=owncloud.db
# A list of SQL commands that should create a copy or the source database
DUMP_FILE=dump.sql
# A list of `sed` actions to convert the SQLite dump into PostgreSQL
SED_FILE=migrate.sed
# Awk program to create sed actions for the BYTEA
AWK_FILE=migrate.awk

if [ -f $DUMP_FILE ]
then
	rm $DUMP_FILE
fi
if [ -f $SED_FILE ]
then
	rm $SED_FILE
fi

sqlite3 $SRC_FILE .dump > $DUMP_FILE

function to_sed() {
  echo $1 >> $SED_FILE
}

to_sed '#!sed -f'
to_sed '/^PRAGMA/d'
to_sed '/^CREATE TABLE/ {'
to_sed 's/\<UNSIGNED\> *//g'
to_sed 's/\<DATETIME\>/TIMESTAMP/g'
to_sed 's/\<LONGTEXT\>/TEXT/g'
to_sed 's/\<INTEGER\>\([^,]*\)[ ]*AUTOINCREMENT/SERIAL\1/g'
to_sed 's/\<CLOB\>/TEXT/g'
to_sed 's/\<BLOB\>/BYTEA/g'
to_sed '}'

cat $DUMP_FILE | grep '^CREATE TABLE.*\<\(BLOB\|BOOLEAN\)\>'| while read LINE
do
	DB=$( echo $LINE | awk '{print $3}' | sed -e 's/"//g' -e "s/'//g" )
	START_PATT='^INSERT INTO "\?\<'$DB'\>"\?'
	END_PATT=');$'
	export DB START_PATT END_PATT
	echo $LINE | sed -e 's/^CREATE TABLE "\?\<'$DB'\>"\? (//g' -e 's/);$//g' -e 's/([^)]*)//g' | awk -F ' *, *' -f $AWK_FILE >> $SED_FILE
done

to_sed "s/decode(NULL,'hex')/NULL/g"
to_sed 'p'

sed -n -f $SED_FILE -i.bak $DUMP_FILE
