#!/bin/bash

TEST_NUMBER=3
NAME=test
LOG_FILE=stress-test-request.log
if [ -f $LOG_FILE ]
then
	rm $LOG_FILE
fi
if [ $# -ge 1 ]
then
	TEST_NUMBER=$1
fi

for num in `seq 1 $TEST_NUMBER`
do
	echo -n "Creating wiki "$Name" #$num..."
	/usr/bin/time --format=%e --output=$LOG_FILE --append ./create-wiki.sh $num $NAME
	TIME=`tail -n 1 $LOG_FILE`
	echo " ("$TIME"s)"
done
