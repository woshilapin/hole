set terminal png size 1024,768 enhanced font "Helvetica,20"
set output 'display.png'

set yrange [0:1000];
set ylabel "Title on the left";
set ytics 100 nomirror textcolor lt 1;

set y2range [0:8000];
set y2label "Title on the right";
set y2tics 1000 nomirror textcolor lt 3;

unset key;

plot \
	"logs" using 1:2 linetype 1, \
	"logs" using 1:3 linetype 3 axes x1y2
