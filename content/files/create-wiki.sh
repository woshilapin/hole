#!/bin/bash

NUMBER=1
NAME=wiki
if [ $# -ge 1 ]
then
	NUMBER=$1
fi
if [ $# -ge 2 ]
then
	NAME=$2
fi

curl \
	--silent \
	--show-error \
	--user Admin:admin \
	-F "wikiname="$NAME \
	-F "wikinumber="$NUMBER \
	-o data/$NAME.html \
	http://localhost:8080/xwiki/bin/view/WikiStressTestCode/FromRequest
