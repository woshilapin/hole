= Screen backlight problem after suspend on Arch
:author: woshilapin
:email: woshilapin@gmail.com
:date: 03-04-2014
:slug: screen-backlight-problem-after-suspend-on-arch
:category: tech
:lang: en
:tags: archlinux, suspend, asus, graphic

== Introduction
On Arch, it seems that there are often problems with the screen backlight.  For
example, I had a problem when suspending my computer with an almost discharged
battery.  When I plugged my computer, it was fully discharged.  Trying to
restart it from suspend didn't display something on my screen.  Let me explain
all the details of this problem.

=== Environment
First of all, let me describe the environment where it happens.  I am using a
ASUS N750JV computer, with Arch Linux installed on it.  My graphic card is an
Intel; this is the result of `lspci` for the Intel card.

[source,txt]
----
VGA compatible controller: Intel Corporation 4th Gen Core Processor Integrated Graphics Controller (rev 06)
----

=== Description of the problem
I was using my computer on battery and reaching around 5% of battery charge, I
decided to suspend my computer by closing it.  I don't really know what kind of
program do the suspend by default on Arch Linux with Gnome.

However, before I can plugged my computer, the battery fully discharge.  In an
ideal case, the computer would have detected that it has only a few minutes to
shutdown in a clean way.  But I am not really sure that Arch (or Gnome, I don't
really know which component should be in charge of this) is doing it right.

When I tried to reboot the computer, the screen never shows me anything.  After
a few try to hard reboot (holding the power button then reboot), I resigned.

== The computer is started
However, I discovered something interesting, the computer seems to start; I had the
first initializations steps displayed.  After these steps, the screen shutdown.
Based on the assumption that the computer is started and that only the screen
didn't work, I tried to blindly connect my session then run a terminal then try
to reboot.  And it works!

== Temporary solution
That's a good news, because I can try to fix it, even if it's blindly.  I tried
a lot of things I found on Internet to switch on backlight with no success.

The only final idea I had was: ``I may try to restart properly the suspend
procedure''.  So instead of trying to swith on the screen backlight, I tried to
suspend my computer in CLI because I suspected than my problem came from a bad
restart after the suspend.

[src,bash]
----
sudo systemctl suspend
----

The computer effectively suspended, the hard drive stopped, it works!  Now,
typing space to restart it... and... magic!  The screen came back to life.

However, first thing I tried is to close my computer then open it to check if it
works well.  That's a deception because, I'm back to my initial problem.  Trying
to reboot it: same problem.  This solution is only a temporary solution to debug
but the problem is still there.

== The solution
Now, I'm pretty sure that it's a only a problem of my screen backlight.  I then
looked the specific Arch wiki page about
link:https://wiki.archlinux.org/index.php/Backlight[backlight].  And the
solution is in the `brightness` file.

There is a `brightness` file under `/sys/class/backlight`.  This brightness only
contains a number with the intensity of the brightness of the screen.  I have
three different subdirectories in the `backlight` directory so I checked the
`brightness` file in each of them (`acpi_video0`, `acpi_video1` and
`intel_backlight`).  And the one in `acpi_video1` was containing the value `0`.

At this stage, be careful because you can't just put any value in this file
without knowing what the maximum value.  In the same folder than your
`brightness` file, you should find the `max_brightness` file which contains the
maximum allowed value.  For example, for my `acpi_video1` directory, it's `100`
but for my `intel_backlight` directory, it's `5273`.  Now, you can replace the
value in the `brightness` file.

[source,bash]
----
echo 100 > /sys/class/backlight/acpi_video1/brightness
----

== Bonus
=== Check you computer is started
That's something I didn't tested, because I was lacking the tools.  There is two
ways I learned.

The first one is easy, you just plug another screen on VGA/DVI/DisplayPort.  If
it works, it's a proof that your computer is started.  However, it doesn't work,
it doesn't proove anything; it's possibly one of the endless problem of
connecting a second screen on Linux!

A more tricky one is using a light.  The hypothesis is that your screen is
working, the only thing that is not working is the backlight of the screen.  In
a black room, using the light with the right angle on your screen, you should
see the LCD pixels displaying things.

== References

- link:https://wiki.archlinux.org/index.php/Backlight[Backlight page for Arch
  Linux]
